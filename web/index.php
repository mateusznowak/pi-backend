<?php

$app = include_once '../src/boot.php';
$app['debug'] = false;
$app->register(new \Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => '../var/prod.log'
));

$app['htttp_cache.ttl'] = 3600;
$app['http_cache']->run();