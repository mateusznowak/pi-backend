$(function() {
    var maxHeight = 0;

    var updateCartItems = function() {
        $.get('/index_dev.php/cart/count')
            .done(function(c) {
                $('.cart-panel .c').text(c);
            });
    };

    $('.featured-products-container > div, .category-products > div').each(function() {
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
        }
    });

    $('.featured-products-container > div, .category-products > div').height(maxHeight);
    $('.btn-add-to-cart').on('click', function() {
        var $el = $(this);
        var productId = $(this).attr('data-id');

        var performDone = function() {
            $el
                .text('Dodano do koszyka')
                .addClass('done');

                updateCartItems();
        };

        $el.attr('disabled', 'disabled').text('Dodawanie...');

        $.post('/index_dev.php/cart/add', {
                'id': productId
            })
            .done(function() {
                performDone();
            })
            .fail(function() {
                throw "Błąd podczas połączenia";
            });
    });
});