$(function() {

    $.getJSON('/index_dev.php/products/visited.json')
        .done(function(jsonResponse) {
            var $el = $('.container-watched');

            if (jsonResponse.length == 0) {
                $el.hide();
                return;
            }

            var template = _.template($('#template-watched').html());

            $el.html(template({
                'products': jsonResponse
            })).removeClass('loading');
        })
});