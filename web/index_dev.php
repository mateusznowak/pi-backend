<?php

$app = include_once '../src/boot.php';
$app['debug'] = true;
$app->register(new \Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => '../var/dev.log'
));

$app['http_cache.ttl'] = 10;
// $app['http_cache']->run();
$app->run();