<?php

namespace ApiClientBundle\Builder\Order;

use FrontendBundle\Models\CartItem;
use JMS\Serializer\Annotation;

class Order
{
    /**
     * @Annotation\Type("string")
     */
    protected $name;

    /**
     * @Annotation\Type("string")
     */
    protected $email;

    /**
     * @Annotation\Type("string")
     */
    protected $phone;

    /**
     * @Annotation\Type("string")
     */
    protected $address;

    /**
     * @Annotation\Type("array")
     */
    protected $productRefs = array();

    /**
     * @Annotation\Type("string")
     */
    protected $apiClientName;

    public function __construct($name, $email, $phone, $zip, $city, $street, $apiClientName, $productRefs)
    {
        $this->name = $name;
        $this->productRefs = $productRefs;
        $this->email = $email;
        $this->phone = $phone;
        $this->apiClientName = $apiClientName;

        $this->buildAddress($zip, $city, $street);
    }

    public function addProductReference(CartItem $productCartItem)
    {
        $this->productRefs[] = $productCartItem->getId();
    }

    protected function buildAddress($zip, $city, $street)
    {
        $this->address = sprintf('%s, %s, %s', $zip, $city, $street);
    }
}