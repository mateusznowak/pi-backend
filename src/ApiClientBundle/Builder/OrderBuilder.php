<?php

namespace ApiClientBundle\Builder;

use ApiClientBundle\Builder\Order\Order as OrderItem;
use ApiClientBundle\Services\ApiClientService;
use FrontendBundle\Models\CartItem;
use FrontendBundle\Models\Order;

class OrderBuilder
{
    /** @var Product[] $orderProductsCollection */
    protected $orderProductsCollection = array();

    /** @var Order $orderDetails  */
    protected $orderDetails;

    public function setOrderDetails(Order $order)
    {
        $this->orderDetails = $order;
    }

    public function addProduct(CartItem $product)
    {
        $this->orderProductsCollection[] = $product;
    }

    public function build($apiClientName)
    {
        $productRefs = array();

        $order = new OrderItem(
            $this->orderDetails->getName(), $this->orderDetails->getEmail(), $this->orderDetails->getPhone(),
            $this->orderDetails->getZip(), $this->orderDetails->getCity(), $this->orderDetails->getStreet(),
            $apiClientName, $productRefs
        );

        foreach ($this->orderProductsCollection as $productCartItem) {
            $order->addProductReference($productCartItem);
        }

        return $order;
    }
}
