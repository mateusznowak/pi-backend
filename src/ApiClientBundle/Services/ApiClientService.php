<?php

namespace ApiClientBundle\Services;

use ApiClientBundle\Builder\Order\Order;
use Buzz\Browser;
use FrontendBundle\Models\CartItem;
use FrontendBundle\Models\Category;
use JMS\Serializer\SerializerInterface;

class ApiClientService
{
    /** @var SerializerInterface */
    protected $serializer;

    /** @var Browser */
    protected $browser;
    protected $uri;
    protected $authenticationToken;

    public function __construct(Browser $browser, SerializerInterface $serializer)
    {
        $this->browser = $browser;
        $this->serializer = $serializer;
    }

    public function getCategories()
    {
        $request = $this->browser->get($this->buildPath('categories'));
        $response = $request->getContent();

        return $this->serializer->deserialize($response, 'array<FrontendBundle\Models\Category>', 'json');
    }

    public function getCategory($category)
    {
        $request = $this->browser->get($this->buildPath('category', '/' . $category));
        $response = $request->getContent();

        return $this->serializer->deserialize($response, 'FrontendBundle\Models\Category', 'json');
    }

    public function getCategoryProducts(Category $category, $limit = null)
    {
        $request = $this->browser->get($this->buildPath('products', '/' . $category->getId()));
        $response = $request->getContent();

        return $this->serializer->deserialize($response, 'array<FrontendBundle\Models\Product>', 'json');
    }

    public function sendOrder(Order $order)
    {
        $serializedOrder = $this->serializer->serialize($order, 'json');

        return $this->browser->post($this->buildPath('order'), array(), $serializedOrder);
    }

    public function getBestseller()
    {
        $request = $this->browser->get($this->buildPath('products'));
        $response = $request->getContent();

        return $this->serializer->deserialize($response, 'array<FrontendBundle\Models\Product>', 'json');
    }

    public function getProducts()
    {
        $request = $this->browser->get($this->buildPath('products'));
        $response = $request->getContent();

        return $this->serializer->deserialize($response, 'array<FrontendBundle\Models\Product>', 'json');
    }

    public function getCartProducts(array $ids)
    {
        if (count($ids) == 0) {
            return array();
        }

        $request = $this->browser->get($this->buildPath('cart', '/' . implode(',', array_keys($ids))));
        $response = $request->getContent();

        $cartItemsList = $this->serializer->deserialize($response, 'array<FrontendBundle\Models\CartItem>', 'json');

        /** @var CartItem $cartItem */
        foreach ($cartItemsList as $cartItem) {
            if (array_key_exists($cartItem->getId(), $ids)) {
                $cartItem->setQuantity($ids[$cartItem->getId()]);
            }
        }

        return $cartItemsList;
    }

    public function getProduct($id)
    {
        $request = $this->browser->get($this->buildPath('product', '/' . $id));
        $response = $request->getContent();

        return $this->serializer->deserialize($response, 'FrontendBundle\Models\Product', 'json');
    }

    public function getFeatured()
    {
        $request = $this->browser->get($this->buildPath('featured'));
        $response = $request->getContent();

        return $this->serializer->deserialize($response, 'array<FrontendBundle\Models\Product>', 'json');
    }

    public function setAuthenticationToken($authenticationToken)
    {
        $this->authenticationToken = $authenticationToken;
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    protected function buildPath($path, $suffix = null)
    {
        if ($suffix) {
            return $this->uri . $path . '.json' . $suffix . '?pubToken=' . $this->authenticationToken;
        } else {
            return $this->uri . $path . '.json?pubToken=' . $this->authenticationToken;
        }
    }
}