<?php

namespace ApiClientBundle\Services;

use FrontendBundle\Models\Product;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ProductVisitorService
{
    const KEY_SESSION_VISITED = 'productsVisited';

    /** @var SessionInterface $session */
    protected $session;

    /** @var SerializerInterface $serializer */
    protected $serializer;

    public function __construct(SessionInterface $session, SerializerInterface $serializer)
    {
        $this->session = $session;
        $this->serializer = $serializer;
    }

    public function refineVisited()
    {
        $this->session->set(static::KEY_SESSION_VISITED, array());
    }

    public function getVisitedJson($limit = 10)
    {
        return $this->serializer->serialize($this->getVisited($limit), 'json');
    }

    public function markVisited(Product $product)
    {
        if (false == $this->isInStack($product)) {
            $visitedArrayList = $this->getVisitedArrayList();

            $visitedArrayList[] = $product;
            $this->session->set(static::KEY_SESSION_VISITED, $visitedArrayList);
        }
    }

    public function getVisited($limit = 10)
    {
        return array_slice($this->getVisitedArrayList(), 0, $limit);
    }

    protected function isInStack(Product $product)
    {
        foreach ($this->getVisitedArrayList() as $p) {
            /** @var $p Product */
            if ($product == $p) {
                return true;
            }
        }

        return false;
    }

    protected function getVisitedArrayList()
    {
        return (array) $this->session->get(static::KEY_SESSION_VISITED);
    }
}