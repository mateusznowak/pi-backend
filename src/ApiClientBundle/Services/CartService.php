<?php

namespace ApiClientBundle\Services;

use ApiClientBundle\Builder\OrderBuilder;
use FrontendBundle\Models\CartItem;
use FrontendBundle\Models\Order;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService implements \Countable
{
    /** @var SessionInterface $session */
    protected $session;

    /** @var ApiClientService $api */
    protected $api;

    protected $apiClientName;

    protected $cartItemStack = null;

    public function __construct(SessionInterface $session, ApiClientService $api, $apiClientName)
    {
        $this->session = $session;
        $this->apiClientName = $apiClientName;
        $this->api = $api;
    }

    public function changeQuantityFromRequest(Request $request)
    {
        $keyValueQuantity = $request->request->get('c');
        $actualItemArray = $this->getCartItemsArray();

        foreach ($actualItemArray as $item => $q) {
            if (array_key_exists($item, $keyValueQuantity)) {
                $actualItemArray[$item] = $keyValueQuantity[$item];
            }
        }

        $this->refine();

        foreach ($actualItemArray as $item => $quantity) {
            $this->addItem($item, $quantity);
        }
    }

    public function finalize(Order $order)
    {
        $orderBuilder = new OrderBuilder();
        $orderBuilder->setOrderDetails($order);

        foreach ($this->getCartItems() as $cartItem) {
            $orderBuilder->addProduct($cartItem);
        }

        $this->api->sendOrder($orderBuilder->build($this->apiClientName));
        $this->refine();

        return true;
    }

    public function getTotalPrice()
    {
        $sum = 0.0;

        /** @var CartItem $cartItem */
        foreach ($this->cartItemStack as $cartItem)
        {
            $sum += ($cartItem->getQuantity() * $cartItem->getPrice());
        }

        return $sum;
    }

    public function getCartItems()
    {
        if (null === $this->cartItemStack) {
            $this->cartItemStack = $this->api->getCartProducts($this->getCartItemsArray());
        }

        return $this->cartItemStack;
    }

    public function refine()
    {
        return $this->session->set('cartItems', array());
    }

    public function addItem($productId, $productCount = 1)
    {
        $cartItems = $this->getCartItemsArray();

        if ($this->isInStack($productId)) {
            $cartItems[$productId]++;

            return;
        }

        $cartItems[$productId] = (Integer) $productCount;

        $this->session->set('cartItems', $cartItems);
    }

    protected function isInStack($productId)
    {
        return array_key_exists($productId, $this->getCartItemsArray());
    }

    protected function getCartItemsArray()
    {
        return (array) $this->session->get('cartItems');
    }

    public function count()
    {
        return count($this->getCartItemsArray());
    }
}