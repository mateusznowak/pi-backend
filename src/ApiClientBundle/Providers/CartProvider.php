<?php

namespace ApiClientBundle\Providers;

use ApiClientBundle\Services\ApiClientService;
use ApiClientBundle\Services\CartService;
use ApiClientBundle\Services\ProductVisitorService;
use Silex\Application;
use Silex\ServiceProviderInterface;

class CartProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['cart'] = $app->share(function() use ($app) {
            return new CartService($app['session'], $app['api'], 'apiFxExample');
        });

        $app['product.visitor'] = $app->share(function() use ($app) {
            return new ProductVisitorService($app['session'], $app['serializer']);
        });
    }

    public function boot(Application $app)
    {
    }
}