<?php

namespace ApiClientBundle\Providers;

use ApiClientBundle\Services\ApiClientService;
use Silex\Application;
use Silex\ServiceProviderInterface;

class ApiClientProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['api'] = $app->share(function() use ($app) {
            $apiClientService = new ApiClientService($app['buzz'], $app["serializer"]);

            $apiClientService->setAuthenticationToken('123PubToken');
            $apiClientService->setUri('http://auction.dev.pl/index.php/api/');

            return $apiClientService;
        });
    }

    public function boot(Application $app)
    {
    }
}
