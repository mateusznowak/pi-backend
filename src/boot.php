<?php

use Silex\Application;

$loader = include_once '../vendor/autoload.php';

$app = new Application();

Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

// Providers
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new \Cobaia\Buzz\Provider\Silex\BuzzServiceProvider());
$app->register(new \ApiClientBundle\Providers\SerializerProvider());
$app->register(new \ApiClientBundle\Providers\ApiClientProvider());
$app->register(new \ApiClientBundle\Providers\CartProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new \Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => '../src',
));
$app->register(new \Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => '../var/cache/',
));
$app->get('/', function() use ($app) {
    return $app->redirect($app['url_generator']->generate('homepage'));
});

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    $twig->addExtension(new \FrontendBundle\TwigExtension\FormExtension());
    return $twig;
}));

// Controllers
$app->mount('/products', new \FrontendBundle\ControllerProviders\ProductControllerProvider());
$app->mount('/categories', new \FrontendBundle\ControllerProviders\CategoryControllerProvider());
$app->mount('/cart', new \FrontendBundle\ControllerProviders\CartControllerProvider());

return $app;
