<?php

namespace FrontendBundle\ControllerProviders;

use ApiClientBundle\Services\ApiClientService;
use ApiClientBundle\Services\ProductVisitorService;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

class ProductControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function(Application $app) {
            $response = new Response(
                $app['twig']->render('FrontendBundle/Resources/views/Product/index.html.twig', array()), 200, array(
                    'Surrogate-Control' => 'content="ESI/1.0"',
                )
            );
            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('homepage');

        $controllers->get('/visited.json', function(Application $app) {
            /** @var ProductVisitorService $productVisitor */
            $productVisitor = $app['product.visitor'];

            $response = new Response($productVisitor->getVisitedJson(10), 200);

            return $response;
        });

        $controllers->get('/{id}/show', function(Application $app, $id) {
            /** @var ApiClientService $api */
            $api = $app['api'];

            $product = $api->getProduct($id);

            /** @var ProductVisitorService $productVisitor */
            $productVisitor = $app['product.visitor'];
            $productVisitor->markVisited($product);

            $response = new Response(
                $app['twig']->render('FrontendBundle/Resources/views/Product/show.html.twig', array(
                    'product' => $product,
                )), 200, array(
                    'Surrogate-Control' => 'content="ESI/1.0"',
                )
            );
            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('product');

        $controllers->get('/bestseller', function(Application $app) {
            $response = new Response($app['twig']->render('FrontendBundle/Resources/views/Product/featured.html.twig', array(
                'featured' => $app['api']->getBestseller(10),
            )));

            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('product.bestseller');

        $controllers->get('/featured', function(Application $app) {
            /** @var ApiClientService $api */
            $api = $app['api'];

            $response = new Response(
                $app['twig']->render('FrontendBundle/Resources/views/Product/featured.html.twig', array(
                    'featured' => $api->getFeatured(),
                )), 200, array(
                    'Surrogate-Control' => 'content="ESI/1.0"',
                )
            );
            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('products.featured');

        return $controllers;
    }
}