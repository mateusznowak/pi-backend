<?php

namespace FrontendBundle\ControllerProviders;

use ApiClientBundle\Services\CartService;
use JMS\Serializer\SerializerInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->post('/add', function(Application $app) {
            $productId = $app['request']->request->get('id');

            $app['cart']->addItem($productId);

            return $app->json(array(
                'id' => $productId
            ), 200);
        })->bind('cart.add');

        $controllers->get('list.json', function(Application $app) {
            return $app->json($app['cart']->getCartItems(), 200);
        })->bind('cart.listJson');

        $controllers->get('/finalize', function(Application $app) {
            $response = new Response($app['twig']->render('FrontendBundle/Resources/views/Cart/finalize.html.twig', array(
                'cartItems' => $app['cart']->getCartItems(),
                'errors' => array(),
            )));

            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('cart.finalize');


        $controllers->post('/finalize', function(Application $app) {
            /** @var Request $request */
            $request = $app['request'];

            /** @var SerializerInterface $serializer */
            $serializer = $app['serializer'];

            /** @var CartService $cartService */
            $cartService = $app['cart'];

            $orderJson = $serializer->serialize($request->request->get('order'), 'json');
            $finalizeStatus = $cartService->finalize($serializer->deserialize($orderJson, 'FrontendBundle\Models\Order', 'json'));

            if (count($finalizeStatus['errors']) == 0) {
                $app['session']->getFlashBag()->add('success', 'Twoje zamówienie zostało złożone. O jego pomyslnym rozpatrzeniu zostaniesz poinformowany drogą mailową.');

                return $app->redirect($app['url_generator']->generate('homepage'));
            }

            $response = new Response($app['twig']->render('FrontendBundle/Resources/views/Cart/finalize.html.twig', array(
                'cartItems' => $cartService->getCartItems(),
                'errors' => $finalizeStatus['errors'],
            )));

            $response->setTtl($app['http_cache.ttl']);

            return $response;
        });

        $controllers->post('/recalculate', function(Application $app) {
            /** @var Request $request */
            $request = $app['request'];

            /** @var CartService $cartService */
            $cartService = $app['cart'];
            $cartService->changeQuantityFromRequest($request);

            return $app->redirect($app['url_generator']->generate('cart'));
        })->bind('cart.recalculate');

        $controllers->get('/count', function(Application $app) {
            return count($app['cart']);
        })->bind('cart.count');

        $controllers->get('/refine', function(Application $app) {
            $app['cart']->refine();

            return $app->redirect($app['url_generator']->generate('cart'));
        })->bind('cart.refine');

        $controllers->get('/', function(Application $app) {
            $response = new Response($app['twig']->render('FrontendBundle/Resources/views/Cart/index.html.twig', array(
                'cartItems' => $app['cart']->getCartItems(),
                'totalPrice' => $app['cart']->getTotalPrice()
            )));

            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('cart');

        return $controllers;
    }
}