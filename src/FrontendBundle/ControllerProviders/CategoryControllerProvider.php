<?php

namespace FrontendBundle\ControllerProviders;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

class CategoryControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function(Application $app) {
            $response = new Response($app['twig']->render('FrontendBundle/Resources/views/Category/index.html.twig', array(
                'categories' => $app['api']->getCategories(),
            )));

            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('categories');

        $controllers->get('/{slug}', function(Application $app, $slug) {
            $category = $app['api']->getCategory($slug);

            $response = new Response($app['twig']->render('FrontendBundle/Resources/views/Category/show.html.twig', array(
                'category' => $category,
                'products' => $app['api']->getCategoryProducts($category),
            )));

            $response->setTtl($app['http_cache.ttl']);

            return $response;
        })->bind('category');

        return $controllers;
    }
}
