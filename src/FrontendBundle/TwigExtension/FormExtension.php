<?php

namespace FrontendBundle\TwigExtension;

use Twig_Environment;

final class FormExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('displayFormError', array($this, 'displayFormError'))
        );
    }

    public function displayFormError(array $errorList)
    {
    }

    public function getName()
    {
        return 'twig_form_extension';
    }
}