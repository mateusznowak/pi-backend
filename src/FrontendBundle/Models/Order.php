<?php

namespace FrontendBundle\Models;

use JMS\Serializer\Annotation;

class Order
{
    /**
     * @Annotation\Type("string")
     */
    protected $name;

    /**
     * @Annotation\Type("string")
     */
    protected $email;

    /**
     * @Annotation\Type("string")
     */
    protected $phone;

    /**
     * @Annotation\Type("string")
     */
    protected $zip;

    /**
     * @Annotation\Type("string")
     */
    protected $city;

    /**
     * @Annotation\Type("string")
     */
    protected $street;

    public function getName()
    {
        return $this->name;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getStreet()
    {
        return $this->street;
    }
}
