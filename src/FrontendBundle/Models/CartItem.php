<?php

namespace FrontendBundle\Models;

use JMS\Serializer\Annotation;

class CartItem
{
    /**
     * @Annotation\Type("string")
     */
    protected $id;

    /**
     * @Annotation\Type("string")
     */
    protected $title;

    /**
     * @Annotation\Type("string")
     */
    protected $image;

    /**
     * @Annotation\Type("float")
     */
    protected $price;

    /**
     * @Annotation\Type("integer")
     */
    protected $quantity;

    public function getId()
    {
        return $this->id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = (Integer) $quantity;

        return $this;
    }

    public function getPrice()
    {
        return (Float) $this->price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getImage()
    {
        return $this->image;
    }
}