<?php

namespace FrontendBundle\Models;

use JMS\Serializer\Annotation;

class Category
{
    /**
     * @Annotation\Type("string")
     */
    protected $id;

    /**
     * @Annotation\Type("string")
     */
    protected $title;

    /**
     * @Annotation\Type("string")
     */
    protected $slug;

    public function __construct($id, $title, $slug)
    {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }
}