<?php

namespace FrontendBundle\Models;

use JMS\Serializer\Annotation;

class Product
{
    /**
     * @Annotation\Type("string")
     */
    protected $id;

    /**
     * @Annotation\Type("string")
     */
    protected $title;

    /**
     * @Annotation\Type("string")
     */
    protected $image;

    /**
     * @Annotation\Type("string")
     */
    protected $description;

    /**
     * @Annotation\Type("float")
     */
    protected $price;

    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return (Float) $this->price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getImage()
    {
        return $this->image;
    }
}